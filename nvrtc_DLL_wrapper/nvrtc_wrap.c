// Cuda to MinGW Wrapper : CW_ functions
// NVRTC compilation implied (Run-Time Compilation)

#include <stdio.h>
#include <stdlib.h>

#include <cuda.h>
#include <nvrtc.h>

#include "nvrtc_wrap.h"

// ERROR HANDLING MACROS AND FUNCTIONS //
// every error is handled as fatal.

#define CHEK(err)  __chek(err, __FILE__, __LINE__)
#define CHEK2(err) __chek2(err, __FILE__, __LINE__)

static void __chek( CUresult err, const char *file, const int line )
{
if	( err )
	{
	const char *errorStr = NULL;
	cuGetErrorString(err, &errorStr);
	fprintf( stderr, "OUPS err %04d \"%s\" <%s>, line %i\n", err, errorStr, file, line );
	exit(1);
	}
}

static void __chek2( nvrtcResult err, const char *file, const int line )
{
if	( err )
	{
	fprintf( stderr, "GASP err %04d \"%s\" <%s>, line %i\n", err, nvrtcGetErrorString(err), file, line );
	exit(1);
	}
}

// compile kernel source to either cubin or PTX
// compilation option "--gpu-architecture" is automatically included
// calling program may provide other options in opt**, with the count of them in optc
//	https://docs.nvidia.com/cuda/nvrtc/index.html#group__options
// memory allocation for the result is automatically provided (malloc)
static int compile_src( int major, int minor, int binflag, const char *src, int optc, const char **opt, char **resu, size_t *resu_size )
{
// prepare compilation options
char option[256];
snprintf( option, sizeof(option), "--gpu-architecture=%s_%d%d", (binflag?"sm":"compute"), major, minor );
const char * options[64];
options[0] = option;
for	( int i = 0; ( i < optc ) && ( i < 63 ); i++ )
	options[i+1] = opt[i];

// create the "nvrtcprogram" object
nvrtcProgram prog;
CHEK2( nvrtcCreateProgram( &prog, src, "jln", 0, NULL, NULL ) );

// compile
nvrtcResult retval = nvrtcCompileProgram( prog, optc+1, options );

// get the log
size_t logSize;
CHEK2( nvrtcGetProgramLogSize( prog, &logSize ) );
if	( logSize > 1 )
	{
	char *log = (char *)malloc( logSize + 1 );
	CHEK2( nvrtcGetProgramLog( prog, log ) );
	log[logSize] = 0;
	printf("----- nvrtc log begin -----\n%s\n------ nvrtc log end -----\n", log );
	free( log );
	}

// quitter si erreur de compilation
CHEK2( retval );

// recuperer le resultat
char * code;
if	( binflag )
	{
	CHEK2( nvrtcGetCUBINSize( prog, resu_size) );
	code = (char *)malloc( *resu_size );
	CHEK2( nvrtcGetCUBIN( prog, code ) );
	}
else	{
	CHEK2( nvrtcGetPTXSize( prog, resu_size) );
	code = (char *)malloc( *resu_size );
	CHEK2( nvrtcGetPTX( prog, code ) );
	}
*resu = code;
return 0;
}

// EXPORTED FUNCTIONS //

// Detection of the GPU device and connexion to its driver
// The application must keep the device handle,
// and the major and minor Cuda hardware version numbers for future use.
// (Note: this DLL supports only one Cuda compatible (NVIDIA) GPU,
// if several Cuda GPU boards are present, one will be chosen arbitrarily)
ZE_API int CW_init( int * device_handle, int * major, int * minor )
{
int deviceCount;
CHEK( cuInit(0) );
CHEK( cuDeviceGetCount( &deviceCount ) );
if	( deviceCount == 0 )
	{
	fprintf( stderr, "no devices supporting CUDA\n" );
	exit(1);
	}
// device handle
CHEK( cuDeviceGet( device_handle, 0 ) );	// 0 c'est le premier trouve
CHEK( cuDeviceGetAttribute( major, CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR, *device_handle ) );
CHEK( cuDeviceGetAttribute( minor, CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MINOR, *device_handle ) );

return 0;
}

// Get the name of a previously connected device
// The application must first allocate a buffer for the texte, and indicate its size
ZE_API int CW_get_device_name( int device_handle, char * name, int name_size )
{
CHEK( cuDeviceGetName( name, name_size, device_handle ) );
return 0;
}

// Get the value of a device attribute
ZE_API int CW_get_device_attr( int device_handle, CUdevice_attribute index )
{
int attrib;
CHEK( cuDeviceGetAttribute( &attrib, index, device_handle ) );
return attrib;
}

// Compile kernel source code to PTX assembly code (null-terminated ASCII)
// for inspection/documentation purpose
// memory allocation for PTXbuf is included (malloc), application can free it with free()
ZE_API int CW_compile_PTX( int device_handle, const char *src, int optc, const char **opt, char **PTXbuf )
{
int major, minor, retval; size_t PTXsize;
CHEK( cuDeviceGetAttribute( &major, CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR, device_handle ) );
CHEK( cuDeviceGetAttribute( &minor, CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MINOR, device_handle ) );
retval = compile_src( major, minor, 0, src, optc, opt, PTXbuf, &PTXsize );
return retval;
}

// Compile kernel source code to cubin (cuda binary executable) and load it into the device
// memory allocation for cubin is done (malloc), then freed after load
// Application must keep the module pointer, to be able to retrieve kernel function addresses
// Cubin size is provided for information.
ZE_API int CW_compile_load( int device_handle, const char *src, int optc, const char **opt, void **module, size_t *cubin_size )
{
int major, minor, retval; char * cubin;
CHEK( cuDeviceGetAttribute( &major, CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR, device_handle ) );
CHEK( cuDeviceGetAttribute( &minor, CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MINOR, device_handle ) );
// compile
retval = compile_src( major, minor, 1, src, optc, opt, &cubin, cubin_size );
//load
CUcontext context;
CUmodule lemodule;

// ici on pourrait passer des flags au lieu de zero
CHEK( cuCtxCreate( &context, 0, device_handle ) );
// on charge le cubin dans le contexte courant, le resultat est un "module"
// (Note : cette fonction pourrait aussi charger un PTX)
CHEK( cuModuleLoadData( &lemodule, cubin ) );
free(cubin);

*module = (void *)lemodule;

return 0;
}

// Retrieve the address of a previously compiled and loaded kernel function
ZE_API int CW_get_func_addr( void ** func_addr, void * module, const char * func_name )
{
CHEK( cuModuleGetFunction( (CUfunction *)func_addr, module, func_name ) );
return 0;
}

// Allocate device global memory (size in bytes)
ZE_API int CW_malloc( void ** d_A, size_t size )
{
CHEK( cuMemAlloc( (CUdeviceptr *)d_A, size) );
return 0;
}

// free device global memory
ZE_API int CW_free( void * d_A )
{
CHEK( cuMemFree( (CUdeviceptr)d_A) );
return 0;
}

// Copy from Host to Device (size in bytes)
ZE_API int CW_memcpy_HtoD( void * d_A, void * h_A, size_t size )
{
// printf("mcopy %p, %p, %d\n", d_A, h_A, (int)size ); fflush(stdout);
CHEK( cuMemcpyHtoD( (CUdeviceptr)d_A, h_A, size ) );
return 0;
}

// Copy from Device to Host (size in bytes)
ZE_API int CW_memcpy_DtoH( void * h_A, void * d_A, size_t size )
{
CHEK( cuMemcpyDtoH( h_A, (CUdeviceptr)d_A, size ) );
return 0;
}

// run many threads of a kernel function in parallel (non-cooperative threads)
//	gx, gy, gz : grid dimensions
//	bx, by, bz : block dimensions
//	shared_size : dynamic shared-memory size per thread block in bytes
//	args : array of pointers to the arguments of the function
ZE_API int CW_launch(   void * func_addr,
			unsigned int gx, unsigned int gy, unsigned int gz,
			unsigned int bx, unsigned int by, unsigned int bz,
			unsigned int shared_size,
			void * stream,
			void ** args,     
			void ** extra )
{
CHEK( cuLaunchKernel( (CUfunction)func_addr, gx, gy, gz, bx, by, bz, shared_size, stream, args, extra ) );
return 0;
}

// wait for termination of the current context tasks
ZE_API int CW_wait_sync()
{
CHEK( cuCtxSynchronize() );
return 0;
}




