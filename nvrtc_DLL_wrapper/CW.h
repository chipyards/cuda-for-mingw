#include <windows.h>
#include <winbase.h>

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

#include "nvrtc_wrap.h"

// a class for C++ access to the "Cuda for Mingw" nvrtc wrapper DLL
// this is to be included by the application program to be compiled with MinGW64
class CW_t {
public:
// DLL import error handling
const char * DLL_last_error = "";
// pointers to the imported functions
CW_init_type  		init;
CW_get_device_name_type get_device_name;
CW_get_device_attr_type	get_device_attr;
CW_compile_PTX_type	compile_PTX;
CW_compile_load_type	compile_load;
CW_get_func_addr_type	get_func_addr;
CW_malloc_type		malloc;
CW_free_type		free;
CW_memcpy_HtoD_type	memcpy_HtoD;
CW_memcpy_DtoH_type	memcpy_DtoH;
CW_launch_type		launch;
CW_wait_sync_type	wait_sync;

// import all functions
int DLL_import( const char * dll_name )
{
struct stat dll_stat;
// check DLL existence
if 	( stat( dll_name, &dll_stat ) )
	{ DLL_last_error = "DLL file not found\n"; return 1; }
// load the DLL
HINSTANCE libHandle = LoadLibrary( dll_name );
if	( libHandle == NULL )
	{ DLL_last_error = "DLL found but loading failed\n"; return 2; }
this->init = (CW_init_type) GetProcAddress( libHandle, "CW_init" ); 
if	( this->init == NULL )
	{ DLL_last_error = "getting CW_init address failed"; return 3; }
this->get_device_name = (CW_get_device_name_type) GetProcAddress( libHandle, "CW_get_device_name" ); 
if	( this->get_device_name == NULL )
	{ DLL_last_error = "getting CW_get_device_name address failed"; return 4; }
this->get_device_attr = (CW_get_device_attr_type) GetProcAddress( libHandle, "CW_get_device_attr" ); 
if	( this->get_device_name == NULL )
	{ DLL_last_error = "getting CW_get_device_attr address failed"; return 5; }
this->compile_PTX = (CW_compile_PTX_type) GetProcAddress( libHandle, "CW_compile_PTX" ); 
if	( this->compile_PTX == NULL )
	{ DLL_last_error = "getting CW_compile_PTX address failed"; return 6; }
this->compile_load = (CW_compile_load_type) GetProcAddress( libHandle, "CW_compile_load" ); 
if	( this->compile_load == NULL )
	{ DLL_last_error = "getting CW_compile_load address failed"; return 7; }
this->get_func_addr = (CW_get_func_addr_type) GetProcAddress( libHandle, "CW_get_func_addr" ); 
if	( this->get_func_addr == NULL )
	{ DLL_last_error = "getting CW_get_func_addr address failed"; return 8;	}
this->malloc = (CW_malloc_type) GetProcAddress( libHandle, "CW_malloc" ); 
if	( this->malloc == NULL )
	{ DLL_last_error = "getting CW_malloc address failed"; return 9; }
this->free = (CW_free_type) GetProcAddress( libHandle, "CW_free" ); 
if	( this->free == NULL )
	{ DLL_last_error = "getting CW_free address failed"; return 10; }
this->memcpy_HtoD = (CW_memcpy_HtoD_type) GetProcAddress( libHandle, "CW_memcpy_HtoD" ); 
if	( this->memcpy_HtoD == NULL )
	{ DLL_last_error = "getting CW_memcpy_HtoD address failed"; return 11; }
this->memcpy_DtoH = (CW_memcpy_DtoH_type) GetProcAddress( libHandle, "CW_memcpy_DtoH" ); 
if	( this->memcpy_DtoH == NULL )
	{ DLL_last_error = "getting CW_memcpy_DtoH address failed"; return 12; }
this->launch = (CW_launch_type) GetProcAddress( libHandle, "CW_launch" ); 
if	( this->launch == NULL )
	{ DLL_last_error = "getting CW_launch address failed"; return 13; }
this->wait_sync = (CW_wait_sync_type) GetProcAddress( libHandle, "CW_wait_sync" ); 
if	( this->wait_sync == NULL )
	{ DLL_last_error = "getting CW_wait_sync address failed"; return 14; }
return 0;
};	// end of DLL_IMPORT
};	// end of CW_t class