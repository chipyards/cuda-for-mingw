// Cuda to MinGW Wrapper : CW_ functions
// NVRTC compilation implied (Run-Time Compilation)

// Windows DLL specific function linkage declaration
#ifdef ZE_EXPORTS
#define ZE_API __declspec(dllexport)
#endif
#ifdef ZE_IMPORTS
#define ZE_API __declspec(dllimport)
#endif

#ifndef __cuda_cuda_h__
// this is provided to let people compile application programs without cuda.h
#include "device_attributes.h"
#endif

// Exported functions declarations
// Every function returns a status int (0 meaning success)
// Every function prototype comes with a type definition for a pointer onto it
// which can be used for run time linking to the DLL

#ifdef  __cplusplus
extern "C" {
#endif

#if ( defined ZE_EXPORTS ) || ( defined ZE_IMPORTS )
// Detection of the GPU device and connexion to its driver
// The application must keep the device handle,
// and the major and minor Cuda hardware version numbers for future use.
// (Note: this DLL supports only one Cuda compatible (NVIDIA) GPU,
// if several Cuda GPU boards are present, one will be chosen arbitrarily)
ZE_API int CW_init( int * device_handle, int * major, int * minor );

// Get the name of a previously connected device
// The application must allocate a buffer for the texte, and indicate its size
ZE_API int CW_get_device_name( int device_handle, char * name, int name_size );

// Get the value of a device attribute
ZE_API int CW_get_device_attr( int device_handle, CUdevice_attribute index );

// compile kernel source code to PTX assembly code (null-terminated ASCII)
// memory allocation for PTXbuf is included (malloc), application can free it with free()
ZE_API int CW_compile_PTX( int device_handle, const char *src, int, const char **, char **PTXbuf );

// compile kernel source code to cubin (cuda binary executable) and load it into the device
// memory allocation for cubin is done (malloc), then freed after load
// Application must keep the module pointer, to be able to retrieve kernel function addresses
// Cubin size is provided for information.
ZE_API int CW_compile_load( int device_handle, const char *src, int, const char **, void **module, size_t *cubin_size );

// Retrieve the address of a previously compiled and loaded kernel function
ZE_API int CW_get_func_addr( void ** func_addr, void * module, const char * func_name );

// Allocate device global memory (size in bytes)
ZE_API int CW_malloc( void ** d_A, size_t size );

// free device global memory
ZE_API int CW_free( void * d_A );

// Copy from Host to Device (size in bytes)
ZE_API int CW_memcpy_HtoD( void * d_A, void * h_A, size_t size );

// Copy from Device to Host (size in bytes)
ZE_API int CW_memcpy_DtoH( void * h_A, void * d_A, size_t size );

// run many threads of a kernel function in parallel (non-cooperative threads)
//	gx, gy, gz : grid dimensions
//	bx, by, bz : block dimensions
//	shared_size : dynamic shared-memory size per thread block in bytes
//	args : array of pointers to the arguments of the function
ZE_API int CW_launch(   void * func_addr,
			unsigned int gx, unsigned int gy, unsigned int gz,
			unsigned int bx, unsigned int by, unsigned int bz,
			unsigned int shared_size,
			void * stream,
			void ** args,     
			void ** extra );

// wait for termination of the current context tasks
ZE_API int CW_wait_sync();

#else
// pointer types for run-time linking
typedef int (*CW_init_type)( int *, int *, int * );
typedef int (*CW_get_device_name_type)( int, char *, int );
typedef int (*CW_get_device_attr_type)( int, CUdevice_attribute );
typedef int (*CW_compile_PTX_type)( int, const char *, int, const char **, char ** );
typedef int (*CW_compile_load_type)( int, const char *, int, const char **, void **, size_t * );
typedef int (*CW_get_func_addr_type)( void **, void *, const char * );
typedef int (*CW_malloc_type)( void **, size_t );
typedef int (*CW_free_type)( void * );
typedef int (*CW_memcpy_HtoD_type)( void *, void *, size_t );
typedef int (*CW_memcpy_DtoH_type)( void *, void *, size_t );
typedef int (*CW_launch_type)( void *, unsigned int, unsigned int, unsigned int,
			unsigned int, unsigned int, unsigned int,
			unsigned int, void *, void **, void ** );
typedef int (*CW_wait_sync_type)();

#endif

#ifdef  __cplusplus
}
#endif
