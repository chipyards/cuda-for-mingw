/* g++ -Wall -O3 -I../nvrtc_DLL_wrapper -o root.exe root_n_div.cpp
 *
 */
#include <windows.h>
#include <winbase.h>

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <math.h>
#include <time.h>	// experimental

#include "cli_parse.h"
#include "CW.h"

static void dump_device( CW_t *CW, int device_handle )
{
char device_name[128];
CW->get_device_name( device_handle, device_name, sizeof(device_name) );
printf("GPU is %s\n", device_name );
printf("Number of multiprocessors in the device: %d\n", CW->get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT ) );
printf("Max number of threads per block: %d\n",	CW->get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_BLOCK ) );
printf("Max resident threads per multiprocessor: %d\n", CW->get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_MULTIPROCESSOR ) );
printf("Max x-dimension of a block: %d\n",		CW->get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_X ) );
printf("Max y-dimension of a block: %d\n",		CW->get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Y ) );
printf("Max z-dimension of a block: %d\n",		CW->get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Z ) );
printf("Max x-dimension of a grid: %d\n",		CW->get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_X ) );
printf("Max y-dimension of a grid: %d\n",		CW->get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Y ) );
printf("Max z-dimension of a grid: %d\n",		CW->get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Z ) );
printf("Max bytes of shared RAM available to a block: %d\n", CW->get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK ) );
printf("Max per block shared RAM size supported: %d\n", CW->get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK_OPTIN ) );
printf("Max bytes of shared RAM available to a multiprocessor: %d\n", CW->get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_MULTIPROCESSOR ) );
printf("Memory bytes available for __constant__ variables: %d\n", CW->get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_TOTAL_CONSTANT_MEMORY ) );
printf("Max number of 32-bit registers available to a block: %d\n", CW->get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MAX_REGISTERS_PER_BLOCK ) );
printf("Max number of 32-bit registers available to a multiprocessor: %d\n", CW->get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MAX_REGISTERS_PER_MULTIPROCESSOR ) );
printf("Ratio of float operations performance to double: %d\n", CW->get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_SINGLE_TO_DOUBLE_PRECISION_PERF_RATIO ) );
}

static void * module_compile( CW_t *CW, int device_handle, const char * kernel_src, int opt_ptx, int opt_count, const char ** options )
{
if	( opt_ptx )	// optionnally watch the assemby src for fun
	{
	char * PTXtext = NULL;
	CW->compile_PTX( device_handle, kernel_src, opt_count, options, &PTXtext );
	if	( PTXtext )
		printf("----- PTX code begin -----\n%s\n%s----- PTX code end -----\n", kernel_src, PTXtext );
	}
// compile the kernel binary code
size_t cubin_size;
void * module;
CW->compile_load( device_handle, kernel_src, opt_count, options, &module, &cubin_size );
printf("cubin size : %d\n", (int)cubin_size );
return module;
}

static unsigned long long Xdrand48 = 0x330E;

double my_drand48( void )
{
Xdrand48 *= 0x5DEECE66DLL;
Xdrand48 += 0xB;
Xdrand48 &= 0xFFFFFFFFFFFFLL;
return( (double)Xdrand48 / (double)(0x1000000000000LL) );
}

void my_srand48( long seedval )
{
Xdrand48 = (unsigned long )seedval;
Xdrand48 <<= 16;
Xdrand48 |= 0x330E;
Xdrand48 &= 0xFFFFFFFFFFFFLL;
}

// generate positive random numbers uniformly distributed on a log scale
// between 1e-12 and 1e12 
static void gen_log_random( float * A, int N )
{
double d;
for	( int i = 0; i < N; i++ )
	{
	d = my_drand48();
	d -= 0.5;		// center
	d *= 24.0;		// expand to [-12:12]
	d = pow( 10.0, d );
	A[i] = (float)d;
	}
}

void dump_time( timespec * t )
{
printf("%u s + %d ns\n", (unsigned int)t->tv_sec, (int)t->tv_nsec );
}

double time_diff( timespec * start, timespec * end )
{
timespec temp;
if	( ( end->tv_nsec - start->tv_nsec ) < 0 )
	{
	temp.tv_sec  = end->tv_sec - start->tv_sec - 1;
	temp.tv_nsec = 1000000000 + end->tv_nsec - start->tv_nsec;
	}
else	{
	temp.tv_sec  = end->tv_sec  - start->tv_sec;
	temp.tv_nsec = end->tv_nsec - start->tv_nsec;
	}
double usec = temp.tv_sec * 1000000.0 + (double)temp.tv_nsec * 0.001;
return usec;
}

/* clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time1);
CLOCK_REALTIME, a system-wide realtime clock.
CLOCK_PROCESS_CPUTIME_ID, high-resolution timer provided by the CPU for each process.
CLOCK_THREAD_CPUTIME_ID, high-resolution timer provided by the CPU for each of the threads.
CLOCK_MONOTONIC is affected by NTP's time adjustment (time slewing). It won't jump, however. – 
CLOCK_MONOTONIC_RAW, which really isn't affected by NTP
*/

const char * kernel_src[] = {
// kernel_src[0] : A[i] non optimise
"extern \"C\" __global__ void ker_1( const float *A, float *J, float *K, float *L )\n\
 {\
 int i = blockDim.x * blockIdx.x + threadIdx.x;\n\
 J[i] = 1/sqrt(A[i]);\
 K[i] = sqrt(A[i]);\
 L[i] = 1.0/A[i];\
 }",
// kernel_src[1] : A[i] optimise a la mano
"extern \"C\" __global__ void ker_1( const float *A, float *J, float *K, float *L )\n\
 {\
 int i = blockDim.x * blockIdx.x + threadIdx.x;\n\
 float Ai = A[i];\
 J[i] = 1/sqrt(Ai);\
 K[i] = sqrt(Ai);\
 L[i] = 1.0/Ai;\
 }",
// kernel_src[2] : A[i] optimise par restrict
"extern \"C\" __global__ void ker_1( const float * __restrict__ A, float * __restrict__ J, float * __restrict__ K, float * __restrict__ L )\n\
 {\
 int i = blockDim.x * blockIdx.x + threadIdx.x;\n\
 J[i] = 1/sqrt(A[i]);\
 K[i] = sqrt(A[i]);\
 L[i] = 1.0/A[i];\
 }",
// kernel_src[3] : rsqrtf et sqrtf
"extern \"C\" __global__ void ker_1( const float * __restrict__ A, float * __restrict__ J, float * __restrict__ K, float * __restrict__ L )\n\
 {\
 int i = blockDim.x * blockIdx.x + threadIdx.x;\n\
 float Ai = A[i];\
 J[i] = rsqrtf(Ai);\
 K[i] = sqrtf(Ai);\
 L[i] = fdividef(1.0,Ai);\
 }"
};

const char * usage = "Options:\n\
    -D <DLL pathname>\n\
    -G <grid size>      # total number of threads\n\
    -B <block size>     # number of threads per block\n\
    -O <optimization>   # f|z|a\n\
    -K <kernel src #>   # kernel variant\n\
    -d                  # dump device attributes\n\
    -p                  # dump PTX source code\n\
";

int main( int argc, char ** argv )
{
// 	parsing CLI arguments
cli_parse * lepar = new cli_parse( argc, (const char **)argv, "DGBOK" );
// parsing is done, retrieve the args
const char * val;

// DLL pathname
const char * dll_name = "../nvrtc_DLL_wrapper/nvrtc_dll.dll";
if	( ( val = lepar->get('D') ) )
	dll_name = val;

// grid size (total number of threads)
int gridsize = 128;
if	( ( val = lepar->get('G') ) )
	gridsize = atoi(val);

// block size (number of threads per block)
int threadsPerBlock = 128;	// arbitrary, but better a multiple of 32
if	( ( val = lepar->get('B') ) )
	threadsPerBlock = atoi(val);

// cuda compilation options
char opt_compil_opt = ' ';
if	( ( val = lepar->get('O') ) )
	opt_compil_opt = val[0];

// kernel code variant
int opt_kernel = 0;
if	( ( val = lepar->get('K') ) )
	opt_kernel = atoi(val);
if	( opt_kernel > int( sizeof( kernel_src ) / sizeof( void * ) ) )
	opt_kernel = 0;

// dump device attributes
int opt_device_dump = 0;
if	( lepar->get('d') )
	opt_device_dump = 1;

// produce and print PTX asm source code
int opt_ptx = 0;
if	( lepar->get('p') )
	opt_ptx = 1;

// help
if	( lepar->get('h') )
	{ printf( usage ); return 0; }
// prepare block and grid dimensions
// division with excess rounding to multiple of 32
threadsPerBlock = ( threadsPerBlock + 31 ) / 32;
threadsPerBlock *= 32;
// division with excess rounding to multiple of threadsPerBlock
int blocksPerGrid = ( gridsize + threadsPerBlock - 1 ) / threadsPerBlock;
gridsize = blocksPerGrid * threadsPerBlock;
printf("Grid : %d blocks of %d threads each, total %d threads\n", blocksPerGrid, threadsPerBlock, gridsize );
fflush(stdout);

CW_t CW;

int dll_err = CW.DLL_import( dll_name );
if	( dll_err )
	{
	fprintf( stderr, "trying DLL %s, error: %s\n", dll_name, CW.DLL_last_error );
	if	( dll_err == 2 )
		printf("try export PATH=$PATH:'/c/Program Files/NVIDIA GPU Computing Toolkit/CUDA/v12.8/bin'\n");
	exit(1);
	}

// contact the GPU
int device_handle, major, minor;
CW.init( &device_handle, &major, &minor );
printf("GPU connected, capability : %d%d\n", major, minor ); fflush(stdout);

if	( opt_device_dump )
	dump_device( &CW, device_handle );

// compilation options
// https://docs.nvidia.com/cuda/nvrtc/index.html#group__options
int opt_count = 0;
const char * options[8];
switch	( opt_compil_opt )
	{
	case 'f': options[0] = "--use_fast_math"; opt_count = 1; break;
	case 'z': options[0] = "--ftz=true"; opt_count = 1; break;
	case 'a': options[0] = "--prec-sqrt=false"; options[1] = "--prec-div=false"; opt_count = 2; break;
	default : opt_count = 0;
	}
for	( int i = 0; i < opt_count; i++ )
	printf("   option %s\n", options[i] );

// compile
void * module = module_compile( &CW, device_handle, kernel_src[opt_kernel], opt_ptx, opt_count, options );
// get the function addresses
void * ker_1_func;
CW.get_func_addr( &ker_1_func, module, "ker_1" );

// device is ready,
// prepare the testbench

size_t q_byte = gridsize * sizeof( float );

// Allocate the host input vectors
float *h_A = (float *)malloc(q_byte);
// Allocate the host output vectors
float *h_J = (float *)malloc(q_byte);
float *h_K = (float *)malloc(q_byte);
float *h_L = (float *)malloc(q_byte);
// Allocate the host verification vectors
float *v_J = (float *)malloc(q_byte);
float *v_K = (float *)malloc(q_byte);
float *v_L = (float *)malloc(q_byte);

// Verify that allocations succeeded
if	( h_A == NULL || h_J == NULL || h_K == NULL || h_L == NULL || v_J == NULL || v_K == NULL || v_L == NULL )
	{
	fprintf(stderr, "Failed to allocate host vectors!\n");
	exit(1);
	}

// create random input data
gen_log_random( h_A, gridsize );
// look at the randomness
printf("A = { %g, %g, %g, %g, %g, ... }\n", h_A[0], h_A[1], h_A[2], h_A[3], h_A[4] ); fflush(stdout);

timespec time0, time1, time2, time3;

printf("CUDA kernel launch\n" ); fflush(stdout);

clock_gettime( CLOCK_REALTIME, &time0 );

// Allocate the device input and output vectors
void * d_A,  * d_J,  * d_K,  * d_L;
CW.malloc( &d_A, q_byte );
CW.malloc( &d_J, q_byte );
CW.malloc( &d_K, q_byte );
CW.malloc( &d_L, q_byte );

// Copy the host input vectors to the device memory
CW.memcpy_HtoD( d_A, h_A, q_byte );

// prepare an array of pointers to the arguments of the kernel function
void * args[] = { (void *)(&d_A), (void *)(&d_J), (void *)(&d_K), (void *)(&d_L) };

clock_gettime( CLOCK_REALTIME, &time1 );
// Let's go parallel !
CW.launch( ker_1_func, blocksPerGrid, 1, 1, threadsPerBlock, 1, 1, 0, NULL, args, NULL );
// CPU thread is blocked here
CW.wait_sync();
clock_gettime( CLOCK_REALTIME, &time2 );

// Copy the device result vectors to the host memory.
CW.memcpy_DtoH( h_J, d_J, q_byte );
CW.memcpy_DtoH( h_K, d_K, q_byte );
CW.memcpy_DtoH( h_L, d_L, q_byte );

clock_gettime( CLOCK_REALTIME, &time3 );

printf("GPU total  time %g microseconds\n", time_diff( &time0, &time3 ) );
printf("GPU kernel time %g microseconds\n", time_diff( &time1, &time2 ) );
fflush(stdout);

// do the same calculations with 1 CPU thread
clock_gettime( CLOCK_REALTIME, &time0 );
float A, J, K, L;
for	( int i = 0; i < gridsize; ++i )
	{
	A = h_A[i];
	J = (float)1.0 / (float)sqrt( A );
	K = (float)sqrt( A );
	L = (float)1.0 / A;
	v_J[i] = J;
	v_K[i] = K;
	v_L[i] = L;
	}
clock_gettime( CLOCK_REALTIME, &time1 );

printf("CPU 1 thread    %g microseconds\n", time_diff( &time0, &time1 ) );

// compute rounding errors
double minerrf, maxerrf, errf;
double minerrd, maxerrd, errd;
float predictf; double predictd;
// Verify the rsqrt calculation
minerrf = 1.0; maxerrf = 0.0;
minerrd = 1.0; maxerrd = 0.0;
for	( int i = 0; i < gridsize; ++i )
	{
	predictf = v_J[i];
	predictd = 1.0 / sqrt( (double)h_A[i] );
	errf = ( h_J[i] - predictf ) / predictf;
	errd = ( (double)h_J[i] - predictd ) / predictd;
	if ( errf < minerrf ) minerrf = errf; else if ( errf > maxerrf ) maxerrf = errf;
	if ( errd < minerrd ) minerrd = errd; else if ( errd > maxerrd ) maxerrd = errd;
	}
printf("Relative error vs float  IEEE-754 for 1/sqrt : %g to %g\n", minerrf, maxerrf );
printf("Relative error vs double IEEE-754 for 1/sqrt : %g to %g\n", minerrd, maxerrd );
// Verify the sqrt calculation
minerrf = 1.0; maxerrf = 0.0;
minerrd = 1.0; maxerrd = 0.0;
for	( int i = 0; i < gridsize; ++i )
	{
	predictf = v_K[i];
	predictd = sqrt( (double)h_A[i] );
	errf = ( h_K[i] - predictf ) / predictf;
	errd = ( (double)h_K[i] - predictd ) / predictd;
	if ( errf < minerrf ) minerrf = errf; else if ( errf > maxerrf ) maxerrf = errf;
	if ( errd < minerrd ) minerrd = errd; else if ( errd > maxerrd ) maxerrd = errd;
	}
printf("Relative error vs float  IEEE-754 for sqrt   : %g to %g\n", minerrf, maxerrf );
printf("Relative error vs double IEEE-754 for sqrt   : %g to %g\n", minerrd, maxerrd );
// Verify the 1/x calculation
minerrf = 1.0; maxerrf = 0.0;
minerrd = 1.0; maxerrd = 0.0;
for	( int i = 0; i < gridsize; ++i )
	{
	predictf = v_L[i];
	predictd = 1.0 / (double)h_A[i];
	errf = ( h_L[i] - predictf ) / predictf;
	errd = ( (double)h_L[i] - predictd ) / predictd;
	if ( errf < minerrf ) minerrf = errf; else if ( errf > maxerrf ) maxerrf = errf;
	if ( errd < minerrd ) minerrd = errd; else if ( errd > maxerrd ) maxerrd = errd;
	}
printf("Relative error vs float  IEEE-754 for 1/x    : %g to %g\n", minerrf, maxerrf );
printf("Relative error vs double IEEE-754 for 1/x    : %g to %g\n", minerrd, maxerrd );


return 0;
}
