/* g++ -Wall -O2 -I../nvrtc_DLL_wrapper -o addsub.exe vect_addsub.cpp
 *
 */
#include <windows.h>
#include <winbase.h>

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <math.h>

#include "CW.h"

const char * vectorAdd_kernel_src =
"extern \"C\" __global__ void vectorAdd(const float *A, const float *B, float *C, int N)\n\
{\n\
int i = blockDim.x * blockIdx.x + threadIdx.x;\n\
if	( i < N ) { C[i] = A[i] + B[i]; }\n\
}\n\
extern \"C\" __global__ void vectorSub(const float *A, const float *B, float *C, int N)\n\
{\n\
int i = blockDim.x * blockIdx.x + threadIdx.x;\n\
if	( i < N ) { C[i] = A[i] - B[i]; }\n\
}\n";


int main( int argc, char ** argv )
{
// nom de la DLL
const char * dll_name = "nvrtc_dll.dll";

int vectsize = 512;
if	( argc > 1 )
	vectsize = atoi(argv[1]); 

if	( argc > 2 )
	dll_name = argv[2]; 

CW_t CW;

if	( CW.DLL_import( dll_name ) )
	{
	fprintf( stderr, "trying DLL %s, error: %s\n", dll_name, CW.DLL_last_error );
	exit(1);
	}

// contact the GPU
int device_handle, major, minor;
CW.init( &device_handle, &major, &minor );
printf("GPU connected, capability : %d%d\n", major, minor ); fflush(stdout);

char device_name[128];
CW.get_device_name( device_handle, device_name, sizeof(device_name) );
printf("GPU is %s\n", device_name );

printf("Number of multiprocessors in the device: %d\n", CW.get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT ) );
printf("Max number of threads per block: %d\n",	CW.get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_BLOCK ) );
printf("Max resident threads per multiprocessor: %d\n", CW.get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_MULTIPROCESSOR ) );
printf("Max x-dimension of a block: %d\n",		CW.get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_X ) );
printf("Max y-dimension of a block: %d\n",		CW.get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Y ) );
printf("Max z-dimension of a block: %d\n",		CW.get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Z ) );
printf("Max x-dimension of a grid: %d\n",		CW.get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_X ) );
printf("Max y-dimension of a grid: %d\n",		CW.get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Y ) );
printf("Max z-dimension of a grid: %d\n",		CW.get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Z ) );
printf("Max bytes of shared RAM available to a block: %d\n", CW.get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK ) );
printf("Max per block shared RAM size supported: %d\n", CW.get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK_OPTIN ) );
printf("Max bytes of shared RAM available to a multiprocessor: %d\n", CW.get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_MULTIPROCESSOR ) );
printf("Memory bytes available for __constant__ variables: %d\n", CW.get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_TOTAL_CONSTANT_MEMORY ) );
printf("Max number of 32-bit registers available to a block: %d\n", CW.get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MAX_REGISTERS_PER_BLOCK ) );
printf("Max number of 32-bit registers available to a multiprocessor: %d\n", CW.get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_MAX_REGISTERS_PER_MULTIPROCESSOR ) );
printf("Ratio of float operations performance to double: %d\n", CW.get_device_attr( device_handle, CU_DEVICE_ATTRIBUTE_SINGLE_TO_DOUBLE_PRECISION_PERF_RATIO ) );

// optionnally watch the assemby src for fun
if	( vectsize == 0 )
	{
	char * PTXtext = NULL;
	const char * options[] = { "--use_fast_math" };	// https://docs.nvidia.com/cuda/nvrtc/index.html#group__options
	// CW.compile_PTX( device_handle, vectorAdd_kernel_src, 0, NULL, &PTXtext );
	CW.compile_PTX( device_handle, vectorAdd_kernel_src, 1, options, &PTXtext );
	if	( PTXtext )
		printf("PTX code:\n%s", PTXtext );
	return 0;
	}

// else compile the kernel code
size_t cubin_size;
void * module;

CW.compile_load( device_handle, vectorAdd_kernel_src, 0, NULL, &module, &cubin_size );
printf("cubin size : %d\n", (int)cubin_size );

// get the function addresses
void * vectorAdd_func, * vectorSub_func;
CW.get_func_addr( &vectorAdd_func, module, "vectorAdd" );
CW.get_func_addr( &vectorSub_func, module, "vectorSub" );

// device is ready,
// prepare the testbench

size_t q_byte = vectsize * sizeof( float );

// Allocate the host input vectors A, B
float *h_A = (float *)malloc(q_byte);
float *h_B = (float *)malloc(q_byte);
// Allocate the host output vector C
float *h_C = (float *)malloc(q_byte);

// Verify that allocations succeeded
if	( h_A == NULL || h_B == NULL || h_C == NULL )
	{
	fprintf(stderr, "Failed to allocate host vectors!\n");
	exit(1);
	}

// Initialize the input data
for	( int i = 0; i < vectsize; ++i )
	{
	h_A[i] = rand() / (float)(RAND_MAX);
	h_B[i] = rand() / (float)(RAND_MAX);
	}

// look at the randomness
printf("A = { %.7f, %.7f, %.7f, %.7f, %.7f, ... }\n", h_A[0], h_A[1], h_A[2], h_A[3], h_A[4] );
printf("B = { %.7f, %.7f, %.7f, %.7f, %.7f, ... }\n", h_B[0], h_B[1], h_B[2], h_B[3], h_B[4] );
fflush(stdout);

// Allocate the device input vectors A, B, C
void * d_A,  * d_B,  * d_C;
CW.malloc( &d_A, q_byte );
CW.malloc( &d_B, q_byte );
CW.malloc( &d_C, q_byte );

// Copy the host input vectors A and B to the device memory
CW.memcpy_HtoD( d_A, h_A, q_byte );
CW.memcpy_HtoD( d_B, h_B, q_byte );

// prepare an array of pointers to the arguments of the kernel function
void * args[4];		// C = A + B
args[0] = (void *)(&d_A);
args[1] = (void *)(&d_B);
args[2] = (void *)(&d_C);
args[3] = (void *)(&vectsize);

// prepare block and grid dimensions
int threadsPerBlock = 256;	// arbitrary, but better a multiple of 32
// division with excess rounding
int blocksPerGrid = ( vectsize + threadsPerBlock - 1 ) / threadsPerBlock;

printf("CUDA kernel launch with %d blocks of %d threads\n", blocksPerGrid, threadsPerBlock );
fflush(stdout);
// Let's go parallel !
CW.launch( vectorAdd_func, blocksPerGrid, 1, 1, threadsPerBlock, 1, 1, 0, NULL, args, NULL );
// we could do somthing while waiting... but not much ;-)
CW.wait_sync();
printf("Done\n");
fflush(stdout);

// Copy the device result vector to the host memory.
CW.memcpy_DtoH( h_C, d_C, q_byte );

printf("C = { %.7f, %.7f, %.7f, %.7f, %.7f, ... }\n", h_C[0], h_C[1], h_C[2], h_C[3], h_C[4] );
fflush(stdout);

// Verify the calculation
for	( int i = 0; i < vectsize; ++i )
	{
	if	( fabs( h_A[i] + h_B[i] - h_C[i] ) > 1e-6 )
		{
		fprintf(stderr, "Result verification failed at element %d!\n", i );
		exit(1);
		}
	}
printf("Test 1 (add) PASSED\n");

// prepare another : C = C - A
args[0] = (void *)(&d_C);
args[1] = (void *)(&d_A);
args[2] = (void *)(&d_C);
args[3] = (void *)(&vectsize);

printf("CUDA kernel launch with %d blocks of %d threads\n", blocksPerGrid, threadsPerBlock );
fflush(stdout);
CW.launch( vectorSub_func, blocksPerGrid, 1, 1, threadsPerBlock, 1, 1, 0, NULL, args, NULL );
CW.wait_sync();
printf("Done\n");
fflush(stdout);

// Copy the device result vector to the host memory.
CW.memcpy_DtoH( h_C, d_C, q_byte );
printf("C = { %.7f, %.7f, %.7f, %.7f, %.7f, ... }\n", h_C[0], h_C[1], h_C[2], h_C[3], h_C[4] );
fflush(stdout);

// prepare another : C = C - B
args[0] = (void *)(&d_C);
args[1] = (void *)(&d_B);
args[2] = (void *)(&d_C);
args[3] = (void *)(&vectsize);

printf("CUDA kernel launch with %d blocks of %d threads\n", blocksPerGrid, threadsPerBlock );
fflush(stdout);
CW.launch( vectorSub_func, blocksPerGrid, 1, 1, threadsPerBlock, 1, 1, 0, NULL, args, NULL );
CW.wait_sync();
printf("Done\n");
fflush(stdout);

// Copy the device result vector to the host memory.
CW.memcpy_DtoH( h_C, d_C, q_byte );
printf("C = { %.7f, %.7f, %.7f, %.7f, %.7f, ... }\n", h_C[0], h_C[1], h_C[2], h_C[3], h_C[4] );
fflush(stdout);

// Free device global memory
CW.free( d_A );
CW.free( d_B );
CW.free( d_C );

// Verify the calculation
for	( int i = 0; i < vectsize; ++i )
	{
	if	( fabs( h_C[i] ) > 1e-6 )
		{
		fprintf(stderr, "Result verification failed at element %d!\n", i );
		exit(1);
		}
	}
printf("Test 2 (sub-sub) PASSED\n");

return 0;
}
